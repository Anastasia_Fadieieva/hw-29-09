﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_29_09.PageObjects
{

    public class Registration1PageObject
    {
        private IWebDriver _webDriver;
        private readonly By _buttonSignUp = By.XPath("/html/body/nb-app/nb-home-page/common-react-bridge/div/header/span[1]/button");
        private readonly By _firstName = By.Name("first_name");
        private readonly By _lastName = By.Name("last_name");
        private readonly By _email = By.Name("email");
        private readonly By _password = By.Name("password");
        private readonly By _passwordCobfirm = By.Name("password_confirm");
        private readonly By _phoneNumber = By.Name("phone_number");
        private readonly By _buttonNext = By.XPath("/html/body/nb-app/nb-signup/common-react-bridge/div/div[2]/div/section/section/div[1]/form/section/section/div/div[2]/section/div/section/button");
        private readonly By _code = By.Name("code");

        private readonly By _nameError = By.CssSelector("input[name='first_name']+div[class^='FormErrorText'] > div");
        private readonly By _lastNameError = By.CssSelector("input[name='last_name']+div[class^='FormErrorText'] > div");
        private readonly By _emailError = By.CssSelector("input[name='email']+div[class^='FormErrorText'] > div");
        private readonly By _passwordError = By.CssSelector("input[name='password']+div[class^='FormErrorText'] > div");
        private readonly By _configPasswordError = By.CssSelector("input[name='password_confirm']+div[class^='FormErrorText'] > div");
        private readonly By _phoneError = By.CssSelector("input[name='phone_number']+div[class^='FormErrorText'] > div");

        public Registration1PageObject(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }
        public IWebElement GetButtonSignUp() => _webDriver.FindElement(_buttonSignUp);
        IWebElement GetFirstName() => _webDriver.FindElement(_firstName);
        IWebElement GetLastName() => _webDriver.FindElement(_lastName);
        IWebElement GetEmail() => _webDriver.FindElement(_email);
        IWebElement GetPassword() => _webDriver.FindElement(_password);
        IWebElement GetPasswordCobfirm() => _webDriver.FindElement(_passwordCobfirm);
        IWebElement GetPhoneNumber() => _webDriver.FindElement(_phoneNumber);
        IWebElement GetCode() => _webDriver.FindElement(_code);
        public IWebElement GetButtonNext() => _webDriver.FindElement(_buttonNext);
        public string GetUrl() => _webDriver.Url;
        //errors:
        public string GetErrorFirstName() => _webDriver.FindElement(_nameError).Text;
        public string GetErrorLastName() => _webDriver.FindElement(_lastNameError).Text;
        public string GetErrorMail() => _webDriver.FindElement(_emailError).Text;
        public string GetErrorPassword() => _webDriver.FindElement(_passwordError).Text;
        public string GetErrorConfigPassword() => _webDriver.FindElement(_configPasswordError).Text;
        public string GetErrorPhone() => _webDriver.FindElement(_phoneError).Text;

        public Registration1PageObject Registration(string name, string lastname, string email, string pass, string configPass, string phone)
        {
            var FieldFirstName = GetFirstName();
            FieldFirstName.Click();
            FieldFirstName.SendKeys(name);
            FieldFirstName.SendKeys(Keys.Enter);

            var FieldLastName = GetLastName();
            FieldLastName.Click();
            FieldLastName.SendKeys(lastname);
            FieldLastName.SendKeys(Keys.Enter);

            var FieldMail = GetEmail();
            FieldMail.Click();
            FieldMail.SendKeys(email);
            FieldMail.SendKeys(Keys.Enter);

            var FieldPassword = GetPassword();
            FieldPassword.Click();
            FieldPassword.SendKeys(pass);
            FieldPassword.SendKeys(Keys.Enter);

            if (!configPass.Equals(""))
            {
                var FieldPasswordCobfirm = GetPasswordCobfirm();
                FieldPasswordCobfirm.Click();
                FieldPasswordCobfirm.SendKeys(configPass);
                FieldPasswordCobfirm.SendKeys(Keys.Enter);
            }

            var FieldPhoneNumber = GetPhoneNumber();
            FieldPhoneNumber.Click();
            FieldPhoneNumber.SendKeys(phone);
            FieldPhoneNumber.SendKeys(Keys.Enter);

            return new Registration1PageObject(_webDriver); 
        }

        public void EnterRefferalCode(string codename)
        {
            var fieldCode = GetCode();
            fieldCode.Click();

            fieldCode.SendKeys(codename);
            fieldCode.SendKeys(Keys.Enter);
        }
    }
}
