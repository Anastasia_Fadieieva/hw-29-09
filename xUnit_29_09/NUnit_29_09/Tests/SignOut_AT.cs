﻿using NUnit.Framework;
using NUnit_29_09.PageObjects.AutorizationPO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;
using NUnit_29_09.PageObjects.Settings;
namespace NUnit_29_09
{
    class SignOut_AT
    {
        public IWebDriver driver;
        //public Autorization autorization = new Autorization();
        [SetUp]
        public void ClassName()//https://newbookmodels.com/explore
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://newbookmodels.com/auth/signin");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            AutorizationPageObject autorization = new AutorizationPageObject(driver);
            autorization.Autorization("nasjo@gmail.com", "n49bpxeCYG$A!Lb");
            autorization.GetButtonLogIn().Click();
        }
        [Theory]
        [TestCase()]
        public void CheckPencil()
        {
            //SettingsAccountInfoPageObject settingsPage = new SettingsAccountInfoPageObject(driver);
            //settingsPage.Pencil();
        }

        //[Theory]
        //[TestCase("nasjo@gmail.com", "n49bpxeCYG$A!Lb")]
        //public void CheckSettingsAccount(string name, string password)
        //{
        //    autorization.ClickAndWriteMail(name);
        //    autorization.ClickAndWritePassord(password);
        //    autorization.ClickLogIn();

        //    Thread.Sleep(1000);
        //    IWebElement buttonX = autorization.driver.FindElement(By.XPath("/html/body/nb-app/common-resend-email/common-modal/div/div[2]/div/div[2]"));
        //    buttonX.Click();

        //    autorization.driver.Manage().Window.Maximize();
        //    IWebElement buttonMenu = autorization.driver.FindElement(By.XPath("/html/body/nb-app/ng-component/nb-internal-layout/common-layout/common-header/section/nb-main-header/common-react-bridge/header/div/div/div[2]/div[2]/div"));
        //    buttonMenu.Click();
        //    IWebElement buttonSignOut = autorization.driver.FindElement(By.XPath("/html/body/nb-app/ng-component/nb-internal-layout/common-layout/common-header/section/nb-main-header/common-react-bridge/header/div/div/div[1]/div[1]/div[6]/button"));
        //    buttonSignOut.Click();
        //    Thread.Sleep(1000);
        //    Assert.AreEqual("https://newbookmodels.com/auth/signin", autorization.driver.Url);
        //}
        [TearDown]
        public void Teardown()
        {
            driver.Quit();
        }
    }
}
