﻿using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using NUnit_29_09.PageObjects;
using OpenQA.Selenium.Chrome;
using System;
using NUnit_29_09.PageObjects.AutorizationPO;

namespace NUnit_29_09
{
    class Autorization_AT
    {
        IWebDriver driver;
        string succsessAutor = "https://newbookmodels.com/auth/signin";

        [SetUp]
        public void ClassName()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://newbookmodels.com/auth/signin");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }
        [Theory]
        [TestCase("nasjo@gmail.com", "n49bpxeCYG$A!Lb")]
        public void CheckSuccessAutorization(string name, string password)
        {
            AutorizationPageObject autorization = new AutorizationPageObject(driver);
            autorization.Autorization(name, password);
            autorization.GetButtonLogIn().Click();

            Assert.AreEqual(succsessAutor, autorization.GetUrl());
        }
        [Theory]
        [TestCase("", "", "Required", "Required")]
        public void CheckInvalidAutorizationWithEmptyFields(string name, string password, string excpErrName, string excpErrPass)
        {
            AutorizationPageObject autorization = new AutorizationPageObject(driver);
            autorization.Autorization(name, password);
            autorization.GetButtonLogIn().Click();

            Assert.AreEqual(succsessAutor, autorization.GetUrl());
            Assert.AreEqual(excpErrName, autorization.GetErrorLogin());
            Assert.AreEqual(excpErrPass, autorization.GetErrorPassword());
        }
        [TearDown]
        public void Teardown()
        {
            driver.Quit();
        }
    }
}
