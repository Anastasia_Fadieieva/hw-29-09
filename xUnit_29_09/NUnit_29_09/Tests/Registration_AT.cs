﻿using NUnit.Framework;
using OpenQA.Selenium;
using NUnit_29_09.PageObjects;
using System;
using OpenQA.Selenium.Chrome;

namespace NUnit_29_09
{
    class Registration_AT
    {
        IWebDriver driver;
        
        private readonly string succsessReg = "https://newbookmodels.com/join/company";
        private readonly string errorReg1 = "https://newbookmodels.com/join";
        private readonly string errorReg2 = "https://newbookmodels.com/join/company";
        [SetUp]
        public void ClassName()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://newbookmodels.com/");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }
        [Theory]
        [TestCase("Tanya", "Fadeeva", "ale328748x384zxlz_dream@gmail.com", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", "7708878374", "ego model", "https://egomodels.com.ua/ua/become-a-model/", "Usa")]
        [TestCase("Nastya", "Yurchenko", "anastasi32923xza_dream@gmail.com", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", "7708878374", "ego model", "https://egomodels.com.ua/ua/become-a-model/", "Usa")]
        public void CheckValidRegistration(string name, string lastname, string email, string pass, string configPass, string phone, string companyName, string companySite, string adress)
        {
            Registration1PageObject registration1 = new Registration1PageObject(driver);
            Registration2PageObject registration2 = new Registration2PageObject(driver);
            var buttonSignUp = registration1.GetButtonSignUp();
            buttonSignUp.Click();
            registration1.Registration(name, lastname, email, pass, configPass, phone);
            registration1.GetButtonNext().Click();

            registration2.Registration(companyName, companySite, adress);
            registration2.SelectIndustry();
            registration2.GetButtonFinish().Click();
            
            Assert.AreEqual(succsessReg, registration2.GetUrl());
        }

        [Theory]
        [TestCase("", "", "", "", "", "", "Required", "Required", "Required", "Invalid password format","Invalid phone format")]
        public void CheckValidationWithEmptyFields(string name, string lastname, string email, string pass, string configPass, string phone, string expectedErrorName, string expErrorFirstName, string expErrorMail, string expErrorPass, string expErrorPhone)
        {
            Registration1PageObject registration1 = new Registration1PageObject(driver);
            var buttonSignUp = registration1.GetButtonSignUp();
            buttonSignUp.Click();
            registration1.Registration(name, lastname, email, pass, configPass, phone);
            registration1.GetButtonNext().Click();

            Assert.AreEqual(errorReg1, registration1.GetUrl());
            Assert.AreEqual(expectedErrorName, registration1.GetErrorFirstName());
            Assert.AreEqual(expErrorFirstName, registration1.GetErrorLastName());
            Assert.AreEqual(expErrorMail, registration1.GetErrorMail());
            Assert.AreEqual(expErrorPass, registration1.GetErrorPassword());
            Assert.AreEqual(expErrorPhone, registration1.GetErrorPhone());
        }
        [Theory]
        [TestCase("", "][{}|'HELLO';:", "anastasi323nxknz_dream@gmail.com", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", "7708878374", "Required")]
        [TestCase(" ", "][{}|'HELLO';:", "anastasi323nxknz_dream@gmail.com", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", "7708878374", "This field may not be blank.")]
        public void CheckInvalidFirstName(string name, string lastname, string email, string pass, string configPass, string phone, string expectedErrorFirstName)
        {
            Registration1PageObject registration1 = new Registration1PageObject(driver);
            var buttonSignUp = registration1.GetButtonSignUp();
            buttonSignUp.Click();
            registration1.Registration(name, lastname, email, pass, configPass, phone);
            registration1.GetButtonNext().Click();

            Assert.AreEqual(errorReg1, registration1.GetUrl());
            Assert.AreEqual(expectedErrorFirstName, registration1.GetErrorFirstName());
        }
        [Theory]
        [TestCase("!ANASTASIA!!", "", "anastasi3jdskdk_dream@gmail.com", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", "7708878374", "Required")]
        [TestCase("!ANASTASIA!!", " ", "anastasi3jdskdk_dream@gmail.com", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", "7708878374", "This field may not be blank.")]
        public void CheckInvalidLastName(string name, string lastname, string email, string pass, string configPass, string phone, string expectedErrorLastName)
        {
            Registration1PageObject registration1 = new Registration1PageObject(driver);
            var buttonSignUp = registration1.GetButtonSignUp();
            buttonSignUp.Click();
            registration1.Registration(name, lastname, email, pass, configPass, phone);
            registration1.GetButtonNext().Click();

            Assert.AreEqual(errorReg1, registration1.GetUrl());
            Assert.AreEqual(expectedErrorLastName, registration1.GetErrorLastName());
        }
        [Theory]
        [TestCase("Yura", "Yurchenko", ".@gmail.com", "n4Fjdm73ssYG$A!Lb", "n49bpxeCYG$A!Lb", "7708878374", "Invalid Email")]
        [TestCase("Tester", "Thsmurchenko", "TEST", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", "7708878374", "Invalid Email")]
        [TestCase("L i l i", "Yakitova09", "LILI@0.0", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", "7708878374", "Invalid Email")]
        [TestCase("01234", "38293-1", ";baby11@kh.city", "!!Fjlx5bpxeCYG$A!Lb", "!!Fjlx5bpxeCYG$A!Lb", "7708878374", "Invalid Email")]
        [TestCase("I dont know", "Who i am", "", "27bpxFjeCYG$A!Lb", "27bpxFjeCYG$A!Lb", "7708878374", "Required")]
        public void CheckInvalidMail(string name, string lastname, string email, string pass, string configPass, string phone, string expectedErrorMail)
        {
            Registration1PageObject registration1 = new Registration1PageObject(driver);
            var buttonSignUp = registration1.GetButtonSignUp();
            buttonSignUp.Click();
            registration1.Registration(name, lastname, email, pass, configPass, phone);
            registration1.GetButtonNext().Click();

            Assert.AreEqual(errorReg1, registration1.GetUrl());
            Assert.AreEqual(expectedErrorMail, registration1.GetErrorMail());
        }
        [Theory]
        [TestCase("=yEN_", "&aTLANTIDA.09", "lilia12345678910@gmail.com", "I098hg!", "", "7708878374", "Invalid password format", "Passwords must match")]
        public void CheckMatchPassword(string name, string lastname, string email, string pass, string configPass, string phone, string expectedErrorPass, string excpectedErrorConfPass)
        {
            Registration1PageObject registration1 = new Registration1PageObject(driver);
            var buttonSignUp = registration1.GetButtonSignUp();
            buttonSignUp.Click();
            registration1.Registration(name, lastname, email, pass, configPass, phone);
            registration1.GetButtonNext().Click();

            Assert.AreEqual(errorReg1, registration1.GetUrl());
            Assert.AreEqual(expectedErrorPass, registration1.GetErrorPassword());
            Assert.AreEqual(excpectedErrorConfPass, registration1.GetErrorConfigPassword());
        }
        [Theory]
        [TestCase("ldsk", "\\./.;SOS", "maxim12345678910@mAX.com", "jlalsmsssF0ss", "jlalsmsssF0ss", "7708878374", "Invalid password format")]
        [TestCase("+", "/$#@~=!%^*", "alina12345678910@l.com", "#098JhdL#098JhdL#098JhdL89", "#098JhdL#098JhdL#098JhdL89", "7708878374", "Invalid password format")]
        [TestCase("/WELCOME$#@~=", "home;", "+-@123.com", "testtesttest009", "testtesttest009", "7708878374", "Invalid password format")]
        public void CheckInvalidFormatPassword(string name, string lastname, string email, string pass, string configPass, string phone, string expectedErrorPass)
        {
            Registration1PageObject registration1 = new Registration1PageObject(driver);
            var buttonSignUp = registration1.GetButtonSignUp();
            buttonSignUp.Click();
            registration1.Registration(name, lastname, email, pass, configPass, phone);
            registration1.GetButtonNext().Click();

            Assert.AreEqual(errorReg1, registration1.GetUrl());
            Assert.AreEqual(expectedErrorPass, registration1.GetErrorPassword());
        }
        [TestCase("гЫСЬ)", ":)", "10@gmail.c", "n49bpxeCYG$A!Lb", "", "7708878374", "Passwords must match")]
        public void CheckMatchConfigPassword(string name, string lastname, string email, string pass, string configPass, string phone, string excpectedErrorConfPass)
        {
            Registration1PageObject registration1 = new Registration1PageObject(driver);
            var buttonSignUp = registration1.GetButtonSignUp();
            buttonSignUp.Click();
            registration1.Registration(name, lastname, email, pass, configPass, phone);
            registration1.GetButtonNext().Click();

            Assert.AreEqual(errorReg1, registration1.GetUrl());
            Assert.AreEqual(excpectedErrorConfPass, registration1.GetErrorConfigPassword());
        }
        [TestCase("?", ". . . .", "taras12345678910@gmail.com", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", "873233827", "Invalid phone format")]
        [TestCase("і КЕН СИИИ 'see@gmail.com'", "01/09/2021", "0@gmail.com", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", " ", "Invalid phone format")]
        public void CheckInvalidPhoneNumber(string name, string lastname, string email, string pass, string configPass, string phone, string excpectedErrorNumber)
        {
            Registration1PageObject registration1 = new Registration1PageObject(driver);
            var buttonSignUp = registration1.GetButtonSignUp();
            buttonSignUp.Click();
            registration1.Registration(name, lastname, email, pass, configPass, phone);
            registration1.GetButtonNext().Click();

            Assert.AreEqual(errorReg1, registration1.GetUrl());
            Assert.AreEqual(excpectedErrorNumber, registration1.GetErrorPhone());
        }
        [TestCase("Yura", "Yurchenko", "taras123456789@gmail.com", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", "8732338279", "KDSKjsojd8sd2m")]
        public void CheckReferralCode(string name, string lastname, string email, string pass, string configPass, string phone, string codeName)
        {
            Registration1PageObject registration1 = new Registration1PageObject(driver);
            var buttonSignUp = registration1.GetButtonSignUp();
            buttonSignUp.Click();
            registration1.Registration(name, lastname, email, pass, configPass, phone);
            registration1.EnterRefferalCode(codeName);
            registration1.GetButtonNext().Click();

            Assert.AreEqual(errorReg1, registration1.GetUrl());
        }
        [Theory]
        [TestCase("Tanya", "Fadeeva", "alexopfozxdslmkk879l_dream@gmail.com", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", "7708878374", "", "", "", "", "Required", "Required", "Please choose a location from the suggested addresses. This field doesn’t accept custom addresses, or “#” symbols.", "Required")]
        public void CheckInvalidRegistrationAfterNext(string name, string lastname, string email, string pass, string configPass, string phone, string companyName, string companySite, string adress, string otherName, string excpErrorCompName, string excpErrorCompURL, string ecxpErrorAdress, string ecxpOtherName)
        {
            Registration1PageObject registration1 = new Registration1PageObject(driver);
            var buttonSignUp = registration1.GetButtonSignUp();
            buttonSignUp.Click();
            registration1.Registration(name, lastname, email, pass, configPass, phone);
            registration1.GetButtonNext().Click();

            Registration2PageObject registration2 = new Registration2PageObject(driver);
            registration2.Registration(companyName, companySite, adress);
            registration2.OtherIndustry(otherName);
            registration2.GetButtonFinish();

            Assert.AreEqual(errorReg2, registration2.GetUrl());
            Assert.AreEqual(excpErrorCompName, registration2.GetErrorCompanyName());
            Assert.AreEqual(excpErrorCompURL, registration2.GetErrorCompanySiteError());
            Assert.AreEqual(ecxpErrorAdress, registration2.GetErrorAdress());
            Assert.AreEqual(ecxpOtherName, registration2.GetErrorIndustry());
        }
        [Theory]
        [TestCase("Tanya", "Fadeeva", "alexopfozxk879l_dream@gmail.com", "n49bpxeCYG$A!Lb", "n49bpxeCYG$A!Lb", "7708878374", "", "", "", "Required", "Required", "Please choose a location from the suggested addresses. This field doesn’t accept custom addresses, or “#” symbols.")]
        public void CheckInvalidDataAfterNext(string name, string lastname, string email, string pass, string configPass, string phone, string companyName, string companySite, string adress, string excpErrorCompName, string excpErrorCompURL, string ecxpErrorAdress)
        {
            Registration1PageObject registration1 = new Registration1PageObject(driver);
            var buttonSignUp = registration1.GetButtonSignUp();
            buttonSignUp.Click();
            registration1.Registration(name, lastname, email, pass, configPass, phone);
            registration1.GetButtonNext().Click();

            Registration2PageObject registration2 = new Registration2PageObject(driver);
            registration2.Registration(companyName, companySite, adress);
            registration2.GetButtonFinish();

            Assert.AreEqual(errorReg2, registration2.GetUrl());
            Assert.AreEqual(excpErrorCompName, registration2.GetErrorCompanyName());
            Assert.AreEqual(excpErrorCompURL, registration2.GetErrorCompanySiteError());
            Assert.AreEqual(ecxpErrorAdress, registration2.GetErrorAdress());
        }

        [TearDown]
        public void Teardown()
        {
            driver.Quit();
        }
    }
}