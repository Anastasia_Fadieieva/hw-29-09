﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace NUnit_29_09
{
    class Autorization
    {
        public IWebDriver driver;
        public void OpenCrome()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://newbookmodels.com/auth/signin");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }
        public void ClickAndWriteMail(string name)
        {
            IWebElement firstName = driver.FindElement(By.Name("email"));
            firstName.Click();
            firstName.SendKeys(name);
            firstName.SendKeys(Keys.Enter);
        }
        public void ClickAndWritePassord(string password)
        {
            IWebElement firstName = driver.FindElement(By.Name("password"));
            firstName.Click();
            firstName.SendKeys(password);
            firstName.SendKeys(Keys.Enter);
        }
        public void ClickLogIn()
        {
            IWebElement buttonNext = driver.FindElement(By.XPath("/html/body/nb-app/ng-component/common-react-bridge/div/div[2]/div/form/section/section/div[3]/button"));
            buttonNext.Click();
        }
    }
}
